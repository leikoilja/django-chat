from django.contrib import admin
from django.urls import path, re_path
from chat_app import views as chat_views
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', chat_views.IndexView.as_view(), name='home'),
    path('admin/', admin.site.urls),
    path('logout/', chat_views.user_logout, name='logout'),
    path('chat', login_required(chat_views.ChatRoomView.as_view(), login_url='home'), name='chat'),
    path('dialogs', login_required(chat_views.DialogsView.as_view(), login_url='home'), name='dialogs'),
    re_path(r'^dialogs/create/(?P<user_id>\d+)/$', chat_views.CreateDialogView.as_view(), name='create_dialog'),
    re_path(r'^dialogs/(?P<chat_id>\d+)/$', login_required(chat_views.MessagesView.as_view(), login_url='home'),
            name='messages')
]
