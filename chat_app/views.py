from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.utils import timezone
from django.views import View
from chat_app.models import Chat
from chat_app.forms import MessageForm
import re


class IndexView(View):
    def get(self, request):
        # If user is authenticated - redirect him/her to chat rooms
        if request.user.id:
            return redirect('chat')
        else:
            # else allow user to login / register
            signup_form = UserCreationForm()
            return render(request, 'index.html', {'signup_form': signup_form})

    def post(self, request):
        # If user tries to login
        if request.POST.get('submit') == 'sign_in':
            # First get the username and password supplied
            username = request.POST.get('username')
            password = request.POST.get('password')

            # Authenticate user
            user = authenticate(username=username, password=password)

            # If user exists
            if user:
                # Log the user in
                login(request, user)
                return redirect('chat')
            else:
                return HttpResponse("Invalid login details supplied.")  # TODO: Login error handling

        # If user tries to signup
        elif request.POST.get('submit') == 'sign_up':
            form = UserCreationForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data.get('username')
                raw_password = form.cleaned_data.get('password1')
                user = authenticate(username=username, password=raw_password)
                login(request, user)
                return redirect('home')
            else:
                return HttpResponse("Invalid registration form")  # TODO: Signup error handling


def user_logout(request):
    # Log out the user
    logout(request)
    return redirect('home')


class ChatRoomView(View):
    def get(self, request):
        # Show ChatRoom with all online users
        # TODO List of online users doesnt seem to be working reliably
        active_sessions = Session.objects.filter(expire_date__gte=timezone.now())
        user_id_list = []
        for session in active_sessions:
            data = session.get_decoded()
            user_id_list.append(data.get('_auth_user_id', None))

        # Query all logged in users based on id list
        online_users = User.objects.filter(id__in=user_id_list)
        return render(request, 'chat_app/chat_room.html', {'users': online_users})


class DialogsView(View):
    def get(self, request):
        chats = Chat.objects.filter(members__in=[request.user.id])
        return render(request, 'chat_app/dialogs.html', {'user_profile': request.user, 'chats': chats})


class CreateDialogView(View):
    def get(self, request, user_id):
        # Query all chats where either of users appear and find the common(id)
        ch1 = Chat.objects.filter(members=request.user.id).values_list('id')
        ch2 = Chat.objects.filter(members=user_id).values_list('id')

        # TODO - It's a mess, find a better way to query and extract chat id
        try:
            chat_id = [int(s) for s in re.findall(r'-?\d+\.?\d*', str(set(ch1).intersection(ch2)))][0]
            chat = Chat.objects.get(id=chat_id)
        except IndexError:
            # If chat is not existing - create new
            chat = Chat.objects.create()
            chat.members.add(request.user)
            chat.members.add(user_id)
        return redirect(reverse('messages', kwargs={'chat_id': chat.id}))


class MessagesView(View):
    def get(self, request, chat_id):
        try:
            chat = Chat.objects.get(id=chat_id)
        except Chat.DoesNotExist:
            chat = None

        return render(
            request,
            'chat_app/messages.html',
            {
                'user_profile': request.user,
                'chat': chat,
                'form': MessageForm()
            }
        )

    def post(self, request, chat_id):
        form = MessageForm(data=request.POST)
        if form.is_valid():
            message = form.save(commit=False)
            message.chat_id = chat_id
            message.author = request.user
            message.save()
        return redirect(reverse('messages', kwargs={'chat_id': chat_id}))
