from django.db import models

from django.contrib.auth.models import User
from datetime import datetime

class Chat(models.Model):
    members = models.ManyToManyField(User)

    @models.permalink
    def get_absolute_url(self):
        return 'messages', (), {'chat_id': self.pk }

class Message(models.Model):
    chat = models.ForeignKey(Chat, on_delete=True)
    author = models.ForeignKey(User, on_delete=True)
    message = models.TextField()
    pub_date = models.DateTimeField(verbose_name='Timestamp of message',default=datetime.now)

    class Meta:
        ordering=['pub_date']

    def __str__(self):
        return self.message

